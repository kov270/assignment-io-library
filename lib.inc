section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi + rax], 0
	je .ret
	inc rax
	jmp .loop
    .ret:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rdx, rax
    mov rsi, rdi
    mov rdi, 1
    mov rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push di
    mov rdi, rsp
    call print_string
    pop di
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

_format_uint:
    mov rax, rsi
    dec rdi
    mov byte[rdi], 0
    mov r10, 10
    .loop:
         xor rdx, rdx
	 div r10
	 add dl, '0'
	 dec rdi
	 mov byte[rdi], dl
	 test rax, rax
	 jnz .loop
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rsi, rdi
    mov rdi, rsp
    sub rsp, 21
    call _format_uint
    call print_string
    add rsp, 21
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge print_uint
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
     xor rcx, rcx
    .loop:
    mov al, byte[rdi+rcx]
     mov dl, byte[rsi+rcx]
     cmp al, dl
     jne .not_equals
    inc rcx
     cmp al, 0x00
     je .equals
     jmp .loop
     .equals:
    mov rax, 1
    ret
    .not_equals:
        mov rax, 0
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rsi, rsp
    mov rdi, 0
    mov rdx, 1
    mov rax, 0
    syscall
    test rax, rax
    je .fail
    .success:
        pop rax
	ret
    .fail:
        pop rax
	xor rax, rax
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rdi
    mov r8, rdi
    mov r9, rsi
    .skip:
	call read_char
	test al, al
	jz .fail
	cmp al, 0x20
	je .skip
	cmp al, 0x9
	je .skip
	cmp al, 0xa
	je .skip
    .read:
        dec r9
	jz .fail
	mov byte[r8], al
	inc r8
	call read_char
	test al, al
	jz .end
	cmp al, 0x20
	je .end
	cmp al, 0x9
	je .end
	cmp al, 0xa
	je .end
	jmp .read
    .end:
        mov byte[r8], 0
	pop rax
	mov rdx, r8
	sub rdx, rax
	ret
    .fail:
        add rsp, 8
	xor rax,rax
	xor rdx,rdx
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    .digit:
        xor r8, r8
	mov r8b, byte[rdi]
	cmp r8b, '0'
	jl .end
	cmp r8b, '9'
	jg .end
	sub r8b, '0'
	imul rax, rax, 10
        add rax, r8
	inc rdi
	inc rdx
	jmp .digit
    .end:
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    test rdx, rdx
    jz .end
    inc rdx
    neg rax
    .end:
    	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    call string_length 
    cmp rax, rdx
    jge .fail
    push rax
    .copy:
        mov r8b, byte[rdi]
	mov byte[rsi], r8b
	inc rdi
	inc rsi
	dec rax
        test rax, rax
	jnz .copy
    pop rax
    mov byte[rsi], 0
    ret
    .fail:
        xor rax, rax
        ret
